package pucrs.progoo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author 10049190
 *
 */
public class InterfaceTexto {

	private Acervo acervo;
	private Cadastro cadastro;
	private Emprestimos listaEmp;
	private Usuario user;
	private Scanner in;

	public InterfaceTexto() {
		acervo = new Acervo();
		cadastro = new Cadastro();
		listaEmp = new Emprestimos();
		user = null;
		in = new Scanner(System.in);
	}

	public void carregaDados() {
		System.out.println("Usuarios:" + Usuario.getTotalUsuarios());

		cadastro.cadastrar("1", "Fulano", "01929123132");
		cadastro.cadastrar("2", "Beltrano", "82816612122");
		cadastro.cadastrar("3", "Ciclano", "91277626212");
		cadastro.cadastrar("4", "Huguinho", "28156483929");
		cadastro.cadastrar("5", "Zezinho", "71626846671");
		cadastro.cadastrar("6", "Luizinho", "77691761727");
		cadastro.ordenaNome();

		System.out.println("Usuarios:" + Usuario.getTotalUsuarios());

		acervo.cadastrarObra(new Livro(1, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 2000, 5));
		acervo.cadastrarObra(new Livro(2, "A Origem das Espécies", "Charles Darwin", "Hemus", 2000, 2));
		acervo.cadastrarObra(new Livro(3, "A Arte da Guerra", "Sun Tzu", "Pensamento", 1995, 3));
		acervo.cadastrarObra(new Periodico(6, "Java 2016", "Ed. Minha", 2016, 1, 4));
		// Exemplo do construtor sobrecarregado (assume total = 1)
		acervo.cadastrarObra(new Livro(4, "Memórias Póstumas de Brás Cubas", "Machado de Assis", "Ática", 1998));
		acervo.cadastrarObra(new Livro(5, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 1958, 1));
		acervo.ordenaTitulo();
		// acervo.ordenaCodigo();

		System.out.println("Usuários:");
		cadastro.listarTodos();

		System.out.println();
		System.out.println("Acervo:");
		acervo.listarTodos();
	}

	public void menuPrincipal() {
		int opcao = 0;
		do {

			System.out.println("*** BIBLIOTECA ***");
			System.out.println();

			if (user != null)
				System.out.println("Usuário ativo: " + user.getNome());

			System.out.println("1 - Escolher usuário");
			System.out.println("2 - Listar empréstimos");
			System.out.println("3 - Retirar um livro");
			System.out.println("4 - Devolver um livro");
			System.out.println("0 - Sair");
			System.out.print("Opção: ");

			opcao = in.nextInt();
			
			if(opcao > 1 && user == null) {
				System.out.println("Primeiro selecione um usuário!");
				System.out.println();
				continue;
			}
			
			switch(opcao) {
				
				case 1:
					menuSelecionarUsuario();
					break;
				case 2:
					menuListarEmprestimos();
					break;
				case 3:
					menuEmprestar();
					break;
				case 4:
					menuDevolver();
					break;
					
			}
			System.out.println();
		} while (opcao != 0);
	}

	private void menuListarEmprestimos() {
		ArrayList<Emprestimo> lista = listaEmp.buscarPorUsuario(user);
		System.out.println();
		System.out.println("Empréstimos:");
		exibeEmprestimos(lista);
	}

	private void menuSelecionarUsuario() {
		System.out.println("Usuários:");
		System.out.println(cadastro.listarTodos());
		System.out.print("Selecione: ");
		int codigo = in.nextInt();
		user = selecionaUsuario(codigo);
		if (user == null) {
			System.out.println("Usuário inválido!");
		}
	}

	private void menuEmprestar() {
		System.out.println();
		System.out.println(acervo.listarTodos());
		System.out.print("Obra desejada: ");
		int codobra = in.nextInt();
		Obra obra = acervo.buscarPorCodigo(codobra);
		if (obra == null) {
			System.out.println("Código inválido!");
			return;
		}
		if (obra.emprestar()) {
			int duracao = obra.getDuracaoEmprestimo();
			LocalDate dataEntrega = LocalDate.now().plusDays(duracao);
			listaEmp.criar(user, obra, dataEntrega);
			System.out.println("*** Obra retirada!");
		} else
			System.out.println(">>> ERRO: não foi possível retirar a obra!");
	}
	
	private void menuDevolver() {
		System.out.println();
		System.out.println("Empréstimos:");
		ArrayList<Emprestimo> lista = listaEmp.buscarPorUsuario(user);
		exibeEmprestimos(lista);
		System.out.print("Qual você deseja devolver? ");
		int numEmp = in.nextInt();
		if (numEmp >= 1 && numEmp <= lista.size()) {
			Emprestimo emp = lista.get(numEmp - 1);
			boolean ok = emp.finalizar(LocalDate.now());
			if (ok)
				System.out.println("Empréstimo finalizado!");
			else
				System.out.println(">>> ERRO: Empréstimo já devolvido!");
		} else
			System.out.println(">>> Empréstimo inexistente!");
	}

	private Usuario selecionaUsuario(int codigo) {
		Usuario user = null;
		if (codigo > 0 && codigo <= cadastro.getTotal())
			user = cadastro.buscarCodigo("" + codigo);
		return user;
	}

	private void exibeEmprestimos(ArrayList<Emprestimo> lista) {
		int cont = 1;
		for (Emprestimo emp : lista) {
			System.out.println(cont + ": " + emp);
			cont++;
		}
	}
}
