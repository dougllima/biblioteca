package pucrs.progoo;

public interface Emprestavel {
	public boolean emprestar();
	public boolean devolver();
	public int getDuracaoEmprestimo();
}
