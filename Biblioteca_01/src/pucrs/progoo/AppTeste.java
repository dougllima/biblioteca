package pucrs.progoo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class AppTeste {

	public static void main(String[] args) {
		
		InterfaceTexto gui = new InterfaceTexto();
		gui.carregaDados();
		gui.menuPrincipal();
		
	}			
}
